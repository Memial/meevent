Required installations:

Node+npm:
1. Download and install Node.js along its modules: https://nodejs.org/en/download/
2. Add your node directory to PATH variable (or use full path to your node installation directory when running commands in console).
3. In your console run commands: 'node -v' and 'npm -v' to check if everything is properly installed.
4. If npm is not installed run: 'npm install' (preferably in node directory, if not add it to your PATH variable as well).

PostgreSQL:

1. Download and install PostgreSQL: https://www.postgresql.org/download/
2. Add the install directory to your PATH variable.
3. Open up pgAdmin to edit/create your admin user.
4. Right click under Login/Group roles and select Create>Login/Group Role...
5. Create a role with every privilege selected (you will use this user to run the Spring server)

IntelliJ IDEA:
1. Download and install the IDE at: https://www.jetbrains.com/idea/download/#section=windows

Running the project:
1. Open up the 'meevent' directory in IntelliJ Idea and check the 'resources>application.properties' file if the fields 'spring.datasource.username' and 'spring.datasource.password' match your postgres admin.
2. Run the project.
3. Go to meevent>contact, open terminal there and run 'npm start'.
4. This should open up the React frontend and land you into the home page (if there is any).

