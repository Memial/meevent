import React,{Component} from 'react';
import './App.css';
import AdminHome from "./AdminUserComponents/AdminHome";

import UserList from "./AdminUserComponents/UserList";
import Route from "react-router-dom/es/Route";

import Router from "react-router-dom/es/Router";
import Switch from "react-router-dom/es/Switch";
import createBrowserHistory from 'history/createBrowserHistory'
import AdminUserEdit from "./AdminUserComponents/AdminUserEdit";
import Contact from "./ContactComponents/Contact";
import EventListWrapper from "./UserProfileComponents/EventListWrapper";

const newHistory = createBrowserHistory();

class App extends Component {
  render() {
    return (
        <Router history={newHistory}>
          <Switch>
            <Route path="/admin" exact={true} component={AdminHome}/>
            <Route path="/admin/userlist" exact={true} component={UserList}/>
            <Route path='/admin/userlist/:id' component={AdminUserEdit}/>
            <Route path='/user/events/:id' component={EventListWrapper}/>
            <Route path='/contact' component={Contact}/>
          </Switch>
        </Router>


    );
  }
}

export default App;
