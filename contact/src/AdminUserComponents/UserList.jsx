import React, { Component } from 'react';
import { Button, ButtonGroup, Container, Table } from 'reactstrap';
import { Link } from 'react-router-dom';
import AdminNavBar from "./AdminNavBar";

class UserList extends Component {

  constructor(props) {
    super(props);
    this.state = {userList: [], isLoading: true};
    this.remove = this.remove.bind(this);
  }

  componentDidMount() {
    this.setState({isLoading: true});

    fetch('/admin/users')
    .then(response => response.json())
    .then(data => this.setState({userList: data, isLoading: false}));
  }

  async remove(id) {
    await fetch(`/admin/users/${id}`, {
      method: 'DELETE',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      }
    }).then(() => {
      let updatedUsers = [...this.state.userList].filter(i => i.normal_users_ID !== id);
      this.setState({userList: updatedUsers});
    });
  }

  render() {
    const userList = Array.from(this.state.userList);
    const isLoading=this.state.isLoading;
    if (isLoading) {
      return <p>Loading...</p>;
    }

    const users = userList.map(user => {
     // const address = `${user.first_name || ''} ${user.last_name || ''} ${user.age || ''}`;
      return <tr key={user.normal_users_ID}>
        <td style={{whiteSpace: 'nowrap'}}>{user.first_name}</td>
        <td>{user.last_name}</td>
        <td>{user.age}</td>

        <td>
          <ButtonGroup>
            <Button size="sm" color="primary" tag={Link} to={"/admin/userlist/" + user.normal_users_ID}>Edit</Button>
            <Button size="sm" color="danger" onClick={() => this.remove(user.normal_users_ID)}>Delete</Button>
          </ButtonGroup>
        </td>
      </tr>
    });

    return (
        <div>
          <AdminNavBar/>
          <Container fluid>
            <div className="float-right">
              <Button color="success" tag={Link} to="/admin/userlist/new">Add User</Button>
            </div>
            <h3>Admin userlist page</h3>
            <Table className="mt-4">
              <thead>
              <tr>
                <th width="20%">First Name</th>
                <th width="20%">Last Name</th>
                <th>Age</th>
                <th width="10%">Actions</th>
              </tr>
              </thead>
              <tbody>
              {users}
              </tbody>
            </Table>
          </Container>
        </div>
    );
  }
}

export default UserList;