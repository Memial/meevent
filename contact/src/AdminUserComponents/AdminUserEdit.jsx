import React, { Component } from 'react';
import { Link, withRouter } from 'react-router-dom';
import { Button, Container, Form, FormGroup, Input, Label } from 'reactstrap';
import AdminNavBar from './AdminNavBar';

class AdminUserEdit extends Component {

  emptyItem = {
    first_name: '',
    last_name: '',
    age: '',
    birth_date: '',

  };

  constructor(props) {
    super(props);
    this.state = {
      item: this.emptyItem
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  async componentDidMount() {
    if (this.props.match.params.id !== 'new') {
      const user = await (await fetch(`/admin/users/${this.props.match.params.id}`)).json();
      this.setState({item: user});
    }
  }

  handleChange(event) {
    const target = event.target;
    const value = target.value;
    const name = target.name;
    let item = {...this.state.item};
    item[name] = value;
    this.setState({item});
  }

  async handleSubmit(event) {
    event.preventDefault();
    const {item} = this.state;

    await fetch('/admin/users', {
      method: (item.normal_users_ID) ? 'PUT' : 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(item),
    });
    this.props.history.push('/admin/userlist');
  }

  render() {
    const {item} = this.state;
    const title = <h2>{item.normal_users_ID ? 'Edit User' : 'Add User'}</h2>;

    return <div>
      <AdminNavBar/>
      <Container>
        {title}
        <Form onSubmit={this.handleSubmit}>
          <FormGroup>
            <Label for="first_name">First Name</Label>
            <Input type="text" name="first_name" id="first_name" value={item.first_name || ''}
                   onChange={this.handleChange} autoComplete="first_name"/>
          </FormGroup>
          <FormGroup>
            <Label for="last_name">Last Name</Label>
            <Input type="text" name="last_name" id="last_name" value={item.last_name || ''}
                   onChange={this.handleChange} autoComplete="last_name"/>
          </FormGroup>
          <FormGroup>
            <Label for="age">Age</Label>
            <Input type="number" name="age" id="age" value={item.age || ''}
                   onChange={this.handleChange} autoComplete="age"/>
          </FormGroup>

          <FormGroup>
            <Button color="primary" type="submit">Save</Button>{' '}
            <Button color="secondary" tag={Link} to="/admin/userlist">Cancel</Button>
          </FormGroup>
        </Form>
      </Container>
    </div>
  }
}

export default withRouter(AdminUserEdit);