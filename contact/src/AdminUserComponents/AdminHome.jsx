import React, { Component } from 'react';
import AdminNavBar from './AdminNavBar';
import { Link } from 'react-router-dom';
import { Button, Container } from 'reactstrap';

class AdminHome extends Component {
  render() {
    return (
        <div>
          <AdminNavBar/>
          <Container fluid>
            <Button color="link"><Link to="/admin/userlist/">Manage users</Link></Button>
          </Container>
        </div>
    );
  }
}

export default AdminHome;