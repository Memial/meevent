import React, { Component } from 'react';

class AdminAppUser extends Component {

  state = {
    isLoading: true,
    userList: []
  };

  async componentDidMount() {
    const response = await fetch('http://127.0.0.1:8083/admin/users');
    const body = await response.json();
    this.setState({ userList: body, isLoading: false });
  }

  render() {
    const {userList, isLoading} = this.state;

    if (isLoading) {
      return <p>Loading...</p>;
    }

    return (
        <div className="App">
          <header className="App-header">

            <div className="App-intro">
              <h2>User List</h2>
              {userList.map(user =>
                  <div key={user.normal_users_ID}>
                    {user.first_name}
                  </div>
              )}
            </div>
          </header>
        </div>
    );
  }
}

export default AdminAppUser;