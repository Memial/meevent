import React,{Component} from 'react';
import 'bootstrap/dist/css/bootstrap.css';

export default class Contact extends Component{
  constructor(props) {
    super(props);

    // Poniższe wiązanie jest niezbędne do prawidłowego przekazania `this` przy wywołaniu funkcji
    this.handleClick = this.handleClick.bind(this);
  }

  handleClick(event){
    //event.preventDefault();
    console.log("CO ZA GOWNO Z TEGO REACTA NAWET SIE WYPISAC NIE CHCE ");

   // this.context.router.push("/contact.send");
  }

  render(){
    return(

        <div className="form-group">
          <h3 className="navbar-text">Napisz do nas wiadomosc</h3>
          <form method="post" action="/contact.send">
            <div>
              Adres Email: <input className="form-control form-control-sm"type="text" name="recipient" required></input>
            </div>
            <div>
              Powtorz Email: <input className="form-control form-control-sm" type="text" name="recipientRepeat" required></input>
            </div>
            <div>
            Temat: <input className="form-control form-control-sm" type="text" name="subject"required></input>
            </div>
            <div>
              Tresc: <input className="form-control form-control-lg" type="text" name="message" required></input>
            </div>
            <input type="submit" className="btn btn-primary" onClick={this.handleClick} value="Submit"></input>
          </form>

        </div>


    )
  }

}