import React from "react";
import './EventListItem.css'
import 'bootstrap/dist/css/bootstrap.css';



const EventListItem=(props)=>(
 <li className='eventListItem_wrapper'>

   <img className="eventListItem_image"  alt="BRAK OBRAZKA" src={props.obrazek}/>
       <div>
         <h2 className="eventListItem_title">{props.tytul}</h2>
         <p className="eventListItem_description">{props.opis}</p>
        <button className="btn btn-info btn-lg my-2">Odwiedz strone eventu</button>
       </div>
 </li>

);
export default EventListItem;
