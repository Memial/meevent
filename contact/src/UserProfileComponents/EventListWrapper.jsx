import React from "react";
import EventListItem from "./EventListItem/EventListItem";
import './EventListWrapper.css'

let eventyTestowe=[
  {
    tytul:'EASTROBO_2080',
    opis :'EastROBO is an international event that is held at Białystok University of Technology. It poses a great time for every enthusiast of robotics and technology. On the spot, you can spectate many different types of robots and autonomous devices including even some fights between them! Also, if you\'re interested in seeing someone\'s creativity freed, then it might a place just for you! EastRobo is organised by Mobilne Systemy Inteligentne science club and Student Council. EastRobo is open event, entry for spectators and competitors is free. There\'ll be a place for kids, where they can start their adventure with LEGO and robotics. ',
    obrazek:'https://previews.123rf.com/images/incomible/incomible1907/incomible190700306/127659338-ancient-mosaic-ceramic-tile-pattern-colorful-tessellation-ornament-floral-decorative-texture-.jpg',
  },
  {
    tytul:'FirstInspires',
    opis :'This 2019-2020 season, FIRST RISESM, powered by Star Wars: Force for Change, is setting out to inspire citizens of the galaxy to work together, strengthening and protecting the Force that binds us and creating a place where collaboration and collective wisdom can elevate new ideas and foster growth.',
    obrazek:'jakis link do obrazka chwilowo brak',
  },
  {
    tytul:'BEST Robotics ',
    opis :'Through the manufacturing process of Bot building, students’ imaginations are captured as they design, build and compete with their own robotic creations.',
    obrazek:'https://www.bestrobotics.org/site/flash/BESTRobotics_Logo.jpg',
  }
];
const EventListWrapper=()=>(
    <ul className="eventListWrapper_wrapper">
      {eventyTestowe.map((item=>(
          <EventListItem
          tytul={item.tytul}
          opis={item.opis}
          obrazek={item.obrazek}

          />

      )))}
    </ul>
);
export default EventListWrapper;