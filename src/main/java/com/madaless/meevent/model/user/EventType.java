package com.madaless.meevent.model.user;

public enum EventType {
    LECTURE, COMPETITION
}
