package com.madaless.meevent.model.user;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
@Data
public class Match {
    @Id
    private Long id;
    @OneToOne
    private Team teamA;
    @OneToOne
    private Team teamB;

    private String teamAscore;
    private String teamBscore;
    private Long round;
}
