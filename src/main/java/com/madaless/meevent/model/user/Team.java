package com.madaless.meevent.model.user;

import lombok.Data;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.util.List;
import java.util.Random;

@Entity
@Data
public class Team {

    private static final int MIN = 1000;
    private static final int MAX = 8000;

    @Id
    private Long id;
    @OneToMany(
            mappedBy = "team",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    private List<OurUser> teamMembers;
    // each team has their own access point
    private Integer accessPoint = (Math.toIntExact(this.getId()) + new Random().nextInt((MAX - MIN) + 1) + MIN);
}
