package com.madaless.meevent.model.user;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Entity
@Getter
@Setter
@Data
public class Competition extends Event {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private EventType eventType = EventType.COMPETITION;

    private Long numberOfTeams;
    private Long currentRound;
    @OneToMany
    private List<Match> matchList;


    private Integer minTeamSize = 1;
    private Integer maxTeamSize = 4;


    public Integer getMinTeamSize() {
        return minTeamSize;
    }

    public void setMinTeamSize(Integer minTeamSize) {
        if(minTeamSize <= maxTeamSize)
            this.minTeamSize = minTeamSize;
    }

    public Integer getMaxTeamSize() {
        return maxTeamSize;
    }

    public void setMaxTeamSize(Integer maxTeamSize) {
        if(minTeamSize <= maxTeamSize)
            this.maxTeamSize = maxTeamSize;
    }


}
