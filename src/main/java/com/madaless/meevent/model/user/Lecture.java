package com.madaless.meevent.model.user;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;

@Entity
@Getter
@Setter
@Data
public class Lecture extends Event {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String subject;
    private EventType eventType = EventType.LECTURE;

    @ManyToMany(mappedBy = "lectures")
    private Set<OurUser> users;
}
