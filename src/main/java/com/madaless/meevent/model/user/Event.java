package com.madaless.meevent.model.user;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.Date;

@Entity
@Data
public class Event {
    @Id
    private Long id;
    private String name;
    private String description;
    private Date date;

}
