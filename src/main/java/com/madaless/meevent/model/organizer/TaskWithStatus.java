package com.madaless.meevent.model.organizer;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
@Data
public class TaskWithStatus {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String description;
    private boolean is_done;
    @ManyToOne
    private EventWithTasks event_with_tasks;

}