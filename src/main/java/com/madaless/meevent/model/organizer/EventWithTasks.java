package com.madaless.meevent.model.organizer;

import com.madaless.meevent.model.user.Event;
import com.madaless.meevent.model.user.OurUser;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
@Data
public class EventWithTasks {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    @OneToOne
    private Event event;
    @ManyToOne
    private OurUser user;

}
