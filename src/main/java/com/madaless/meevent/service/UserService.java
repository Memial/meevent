package com.madaless.meevent.service;

import com.madaless.meevent.model.user.OurUser;
import com.madaless.meevent.repository.UserRepository;
import javassist.NotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service

@RequiredArgsConstructor
public class UserService {

    private final UserRepository userRepository;

    public List<OurUser> findAll() {
        return userRepository.findAll();
    }

    public OurUser findById(Long id) {
        return userRepository.findById(id).orElseThrow(() -> new RuntimeException());
    }

    public OurUser save(OurUser stock) {
        return userRepository.save(stock);
    }

    public void deleteById(Long id) {
        userRepository.deleteById(id);
    }
}