package com.madaless.meevent.service;

import com.madaless.meevent.model.organizer.Task;
import com.madaless.meevent.repository.TaskRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service

@RequiredArgsConstructor
public class TaskService {

    private final TaskRepository classRepository;

    public List<Task> findAll() {
        return classRepository.findAll();
    }

    public Optional<Task> findById(Long id) {
        return classRepository.findById(id);
    }

    public Task save(Task stock) {
        return classRepository.save(stock);
    }

    public void deleteById(Long id) {
        classRepository.deleteById(id);
    }
}