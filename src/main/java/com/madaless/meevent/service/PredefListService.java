package com.madaless.meevent.service;

import com.madaless.meevent.model.organizer.PredefList;
import com.madaless.meevent.repository.PredefListRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service

@RequiredArgsConstructor
public class PredefListService {

    private final PredefListRepository classRepository;

    public List<PredefList> findAll() {
        return classRepository.findAll();
    }

    public Optional<PredefList> findById(Long id) {
        return classRepository.findById(id);
    }

    public PredefList save(PredefList stock) {
        return classRepository.save(stock);
    }

    public void deleteById(Long id) {
        classRepository.deleteById(id);
    }
}