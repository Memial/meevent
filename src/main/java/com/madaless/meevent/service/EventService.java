package com.madaless.meevent.service;

import com.madaless.meevent.model.user.Event;
import com.madaless.meevent.repository.EventRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service

@RequiredArgsConstructor
public class EventService {

    private final EventRepository classRepository;

    public List<Event> findAll() {
        return classRepository.findAll();
    }

    public Event findById(Long id) {
        return classRepository.findById(id).orElseThrow(RuntimeException::new);
    }

    public Event save(Event stock) {
        return classRepository.save(stock);
    }

    public void deleteById(Long id) {
        classRepository.deleteById(id);
    }
}