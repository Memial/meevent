package com.madaless.meevent.service;

import com.madaless.meevent.model.organizer.TaskWithStatus;
import com.madaless.meevent.repository.TaskWithStatusRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service

@RequiredArgsConstructor
public class TaskWithStatusService {

    private final TaskWithStatusRepository classRepository;

    public List<TaskWithStatus> findAll() {
        return classRepository.findAll();
    }

    public Optional<TaskWithStatus> findById(Long id) {
        return classRepository.findById(id);
    }

    public TaskWithStatus save(TaskWithStatus stock) {
        return classRepository.save(stock);
    }

    public void deleteById(Long id) {
        classRepository.deleteById(id);
    }
}