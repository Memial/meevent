package com.madaless.meevent.service;

import com.madaless.meevent.model.organizer.EventWithTasks;
import com.madaless.meevent.repository.EventWithTasksRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service

@RequiredArgsConstructor
public class EventWithTasksService {

    private final EventWithTasksRepository classRepository;

    public List<EventWithTasks> findAll() {
        return classRepository.findAll();
    }

    public Optional<EventWithTasks> findById(Long id) {
        return classRepository.findById(id);
    }

    public EventWithTasks save(EventWithTasks stock) {
        return classRepository.save(stock);
    }

    public void deleteById(Long id) {
        classRepository.deleteById(id);
    }
}