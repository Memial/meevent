package com.madaless.meevent.controllers.user;

import com.madaless.meevent.model.user.Event;
import com.madaless.meevent.model.user.OurUser;
import com.madaless.meevent.service.EventService;
import com.madaless.meevent.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

public class UserController {
    @Autowired
    private UserService userService;
    @Autowired
    private EventService eventService;

    @PostMapping("events/{eventId}/users/{userId}")
    public void signUpForEvent(@PathVariable Long eventId, @PathVariable Long userId){
        OurUser user = userService.findById(userId);
        Event event = eventService.findById(eventId);
        // to DO
    }
}
