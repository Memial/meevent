package com.madaless.meevent.controllers.admin.contact;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;

@Controller
@CrossOrigin(origins = {"http://localhost:3000", "http://localhost:8083"})
public class ContactController {


    @Autowired
    private JavaMailSender mailSender;

    @RequestMapping("/contact.send")
    //@CrossOrigin(origins = "http://localhost:3000")
    public String doSendEmail(HttpServletRequest request) {
        String recipientAddress = request.getParameter("recipient");
        if (recipientAddress.compareTo(request.getParameter("recipientRepeat")) != 0) {
            System.out.println("Nie wchodzi");
            return "bagno";
        }
        //System.out.println(" wchodzi");
        String subject = request.getParameter("subject");
        String message = request.getParameter("message");

        // prints debug info
        System.out.println("To: " + recipientAddress);
        System.out.println("Subject: " + subject);
        System.out.println("Message: " + message);

        SimpleMailMessage email = new SimpleMailMessage();
        email.setTo("dodjango69@gmail.com");
        //email.setTo(recipientAddress);
        email.setSubject(subject);
        email.setText(recipientAddress + " pisze\n" + message);

        mailSender.send(email);


        return "result";
    }

    @RequestMapping("/gowno")
    public String doSendEmail() {

        SimpleMailMessage email = new SimpleMailMessage();
        email.setTo("dodjango69@gmail.com");

        email.setSubject("subject");
        email.setText("recipientAddress" + "\n" + "message");

        mailSender.send(email);


        return "result";
    }

}
