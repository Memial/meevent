package com.madaless.meevent.controllers.admin;

import com.madaless.meevent.model.user.OurUser;
import com.madaless.meevent.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Collection;
import java.util.Optional;


@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*", exposedHeaders = "Content-Range", allowCredentials = "true")
@RequestMapping("/admin/users")
public class AdminController {
    private final Logger log = LoggerFactory.getLogger(AdminController.class);
    @Autowired
    private UserRepository userRepository;

    @ModelAttribute
    public void setVaryResponseHeader(HttpServletResponse response) {
    }

    @GetMapping
    @ResponseBody
    public Collection<OurUser> findAll(HttpServletResponse response) {
        response.addHeader("Content-Range", String.valueOf(userRepository.count()));
        return userRepository.findAll();
    }

    @GetMapping("/{id}")
    ResponseEntity<?> getGroup(@PathVariable Long id) {
        Optional<OurUser> users = userRepository.findById(id);
        return users.map(response -> ResponseEntity.ok().body(response))
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @PostMapping
    ResponseEntity<OurUser> createGroup(@Valid @RequestBody OurUser normalUser) throws URISyntaxException {
        log.info("Request to create user: {}", normalUser);
        OurUser result = userRepository.save(normalUser);
        return ResponseEntity.created(new URI("/api/group/" + result.getId()))
                .body(result);
    }

    @PutMapping()
    @ResponseStatus(HttpStatus.OK)
    public void update(@RequestBody OurUser user) {
        if (user != null) {
            if (userRepository.existsById(user.getId())) {
                userRepository.deleteById(user.getId());
                userRepository.save(user);
            }

        }
    }

    @DeleteMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.OK)
    public void delete(@PathVariable("id") Long id) {
        userRepository.deleteById(id);
    }

}
