package com.madaless.meevent;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MeeventApplication {

    public static void main(String[] args) {
        SpringApplication.run(MeeventApplication.class, args);
    }

}
