package com.madaless.meevent.repository;

import com.madaless.meevent.model.organizer.Task;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TaskRepository extends JpaRepository<Task, Long> {
}