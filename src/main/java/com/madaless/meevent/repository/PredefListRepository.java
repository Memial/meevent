package com.madaless.meevent.repository;

import com.madaless.meevent.model.organizer.PredefList;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PredefListRepository extends JpaRepository<PredefList, Long> {
}