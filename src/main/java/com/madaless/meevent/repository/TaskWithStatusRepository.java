package com.madaless.meevent.repository;

import com.madaless.meevent.model.organizer.TaskWithStatus;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TaskWithStatusRepository extends JpaRepository<TaskWithStatus, Long> {
}