package com.madaless.meevent.repository;

import com.madaless.meevent.model.organizer.EventWithTasks;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EventWithTasksRepository extends JpaRepository<EventWithTasks, Long> {
}
