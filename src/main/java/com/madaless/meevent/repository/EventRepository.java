package com.madaless.meevent.repository;

import com.madaless.meevent.model.user.Event;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EventRepository extends JpaRepository<Event, Long> {
}
