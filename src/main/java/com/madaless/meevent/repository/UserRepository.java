package com.madaless.meevent.repository;

import com.madaless.meevent.model.user.OurUser;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<OurUser, Long> {


}